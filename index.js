const productTitle = document.getElementById('productTitle')
const priceSpan = document.getElementById('price')
const productDescription = document.getElementById('productDescription')
const mainImage = document.getElementById('mainImage')
const listingDiv = document.getElementById('listingDiv')


fetch('https://fakestoreapi.com/products/16')
    .then(response => response.json())
    .then((product) => {
        productTitle.innerHTML = product.title
        priceSpan.innerHTML = product.price
        productDescription.innerHTML = product.description
        mainImage.src = product.image
    })
    .catch(error => console.log(error))



fetch('https://fakestoreapi.com/products')
    .then((response) => response.json())
    .then((productsList) => {
        for (let i = 0; i < productsList.length; i++) {
            const product = productsList[i]

            const productArticle = document.createElement('article')
            productArticle.classList.add('product')

            const productImage = document.createElement('img')
            productImage.src = product.image
            productArticle.appendChild(productImage)

            const productDetailsDiv = document.createElement('div')
            productDetailsDiv.classList.add('productDetails')
            productArticle.appendChild(productDetailsDiv)

            const productTitleH3 = document.createElement('h3')
            productTitleH3.classList.add('h6')
            productTitleH3.innerHTML = product.title
            productDetailsDiv.appendChild(productTitleH3)

            const starsDiv = document.createElement('div')
            productDetailsDiv.appendChild(starsDiv)

            const starSpan = document.createElement('span')
            starSpan.innerHTML = '&#9733;'
            starsDiv.appendChild(starSpan)
            const starSpan2 = document.createElement('span')
            starSpan2.innerHTML = '&#9733;'
            starsDiv.appendChild(starSpan2)
            const starSpan3 = document.createElement('span')
            starSpan3.innerHTML = '&#9733;'
            starsDiv.appendChild(starSpan3)
            const starSpan4 = document.createElement('span')
            starSpan4.innerHTML = '&#9733;'
            starsDiv.appendChild(starSpan4)
            const starSpan5 = document.createElement('span')
            starSpan5.innerHTML = '&#9734;'
            starsDiv.appendChild(starSpan5)

            const priceAndButtonDiv = document.createElement('div')
            priceAndButtonDiv.classList.add('priceAndButton')
            productDetailsDiv.appendChild(priceAndButtonDiv)

            const priceSpan = document.createElement('span')
            priceSpan.classList.add('p')
            priceSpan.innerHTML = '$' + product.price
            priceAndButtonDiv.appendChild(priceSpan)

            const button = document.createElement('button')
            button.classList.add('button')
            button.classList.add('buttonPrimary')
            button.innerHTML = 'Add to cart'
            priceAndButtonDiv.appendChild(button)

            listingDiv.appendChild(productArticle)
        }
    })
    .catch(error => console.log(error))